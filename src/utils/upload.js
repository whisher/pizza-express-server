"use strict";

const sharp = require("sharp");

const createThumb = (prefix, imageName) => {
  const imagePath = "./images/";
  const resize = size =>
    sharp(imagePath + imageName)
      .resize(size, size)
      .toFile(imagePath + prefix + size + "_" + imageName);
  return Promise.all([1440, 1080, 720, 480, 200].map(resize))
    .then(() => {
      return imageName;
    })
    .catch(err => {});
};

module.exports = createThumb;
