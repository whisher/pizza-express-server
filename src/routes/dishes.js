"use strict";
const express = require("express");
const router = express.Router();
const { celebrate } = require("celebrate");
const controllers = require("../controllers/dishes");
const validators = require("../validators/dishes");
const jwtVerify = require("../middleware/jwt-verify");

router.post("", jwtVerify, controllers.create);

router.put("/:id", jwtVerify, celebrate(validators.update), controllers.update);

router.get("", jwtVerify, controllers.all);

router.get("/:id", jwtVerify, controllers.getById);

router.get("/category/:id", jwtVerify, controllers.getByCategoryId);

router.delete("/:id", jwtVerify, controllers.delete);

module.exports = router;
