"use strict";
const express = require("express");
const router = express.Router();
const controllers = require("../controllers/places");

router.get("", controllers.all);

router.get("/:name", controllers.getByName);

module.exports = router;
