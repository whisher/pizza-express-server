"use strict";
const express = require("express");
const router = express.Router();
const controllers = require("../controllers/upload");
const jwtVerify = require("../middleware/jwt-verify");
const upload = require("../middleware/upload");

router.post("", jwtVerify, upload, controllers.createThumb);

module.exports = router;
