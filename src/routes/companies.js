"use strict";
const express = require("express");
const router = express.Router();
const { celebrate } = require("celebrate");
const controllers = require("../controllers/companies");
const validators = require("../validators/companies");
const jwtVerify = require("../middleware/jwt-verify");

router.post("", celebrate(validators.create), jwtVerify, controllers.create);

router.put("/:id", celebrate(validators.update), jwtVerify, controllers.update);

router.get("", jwtVerify, controllers.all);

router.get("/:id", jwtVerify, controllers.getById);

router.delete("/:id", jwtVerify, controllers.delete);

module.exports = router;
