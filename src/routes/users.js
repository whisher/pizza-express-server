"use strict";
const express = require("express");
const router = express.Router();
const { celebrate } = require("celebrate");
const controllers = require("../controllers/users");
const validators = require("../validators/users");
const jwtVerify = require("../middleware/jwt-verify");

router.post("", jwtVerify, celebrate(validators.create), controllers.create);

router.put("/:id", jwtVerify, controllers.update);

router.get("", jwtVerify, controllers.all);

router.get("/:id", jwtVerify, controllers.getById);

router.delete("/:id", jwtVerify, controllers.delete);

router.post("/signup", celebrate(validators.signup), controllers.signup);

router.post("/login", celebrate(validators.login), controllers.login);

module.exports = router;
