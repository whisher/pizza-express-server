/*eslint max-len: ["error", { "code": 130 }]*/
"use strict";
const fs = require("fs");
// Jwt
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const config = require("../config/config");
const User = require("../models/users");

const updateUser = (id, data) => {
  return User.findByIdAndUpdate({ _id: id }, data, { new: true });
};

exports.create = (req, res, next) => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(req.body.password, salt);
  const userData = new User({
    avatar: req.body.avatar,
    email: req.body.email,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    mobile: req.body.mobile,
    password: hash,
    role: req.body.role
  });
  userData
    .save()
    .then(user => {
      delete user.password;
      res.status(200).json(user);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.update = (req, res, next) => {
  const userData = {
    avatar: req.body.avatar,
    email: req.body.email,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    mobile: req.body.mobile,
    role: req.body.role
  };
  updateUser(req.params.id, userData)
    .then(user => {
      res.status(200).json(user);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.all = (req, res, next) => {
  User.find()
    .sort({ created: 1 })
    .then(users => {
      res.status(200).json(users);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.getById = (req, res, next) => {
  User.findById(req.params.id)
    .then(user => {
      res.status(200).json(user);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;
  User.findOneAndRemove({ _id: id })
    .then(user => {
      res.status(200).json(user._id);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.signup = (req, res, next) => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(req.body.password, salt);
  const userData = new User({
    email: req.body.email,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    mobile: req.body.mobile,
    password: hash,
    role: "user"
  });
  userData
    .save()
    .then(user => {
      const userData = {
        _id: user._id,
        email: user.email,
        firstname: user.firstname,
        lastname: user.lastname,
        mobile: user.mobile,
        role: user.role
      };
      const token = jwt.sign(userData, config.secret, { expiresIn: "1h" });
      res.status(200).json({
        token: token,
        expiresIn: 3600
      });
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.login = (req, res, next) => {
  User.findOne({ email: req.body.email })
    .then(user => {
      if (!user) {
        return res.status(401).json({
          message: "Auth failed"
        });
      }
      const isValid = bcrypt.compareSync(req.body.password, user.password);
      if (!isValid) {
        return res.status(401).json({
          message: "Auth failed"
        });
      }
      const userData = {
        _id: user._id,
        avatar: user.avatar ? user.avatar : "",
        email: user.email,
        firstname: user.firstname,
        lastname: user.lastname,
        mobile: user.mobile,
        role: user.role
      };
      const token = jwt.sign(userData, config.secret, { expiresIn: "1h" });
      res.status(200).json({
        token: token,
        expiresIn: 3600
      });
    })
    .catch(err => {
      return res.status(500).json({
        message: "Server error"
      });
    });
};
