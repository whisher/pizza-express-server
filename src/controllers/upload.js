"use strict";

const createThumb = require("../utils/upload");

exports.createThumb = (req, res, next) => {
  createThumb("thumb_", req.file.filename)
    .then(imageName => {
      res.status(200).json(imageName);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};
