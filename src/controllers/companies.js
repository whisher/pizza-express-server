/*eslint max-len: ["error", { "code": 130 }]*/
"use strict";

const Company = require("../models/companies");

const updateCompany = (id, data) => {
  return Company.findByIdAndUpdate({ _id: id }, data, { new: true });
};

exports.create = (req, res, next) => {
  const companyData = new Company({
    address: req.body.address,
    image: req.body.image,
    name: req.body.name
  });
  companyData
    .save()
    .then(company => {
      res.status(200).json(company);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.update = (req, res, next) => {
  const companyData = {
    address: req.body.address,
    image: req.body.image,
    name: req.body.name
  };
  updateCompany(req.params.id, companyData)
    .then(company => {
      res.status(200).json(company);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.all = (req, res, next) => {
  Company.find()
    .sort({ created: 1 })
    .then(companies => {
      res.status(200).json(companies);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.getById = (req, res, next) => {
  Company.findById(req.params.id)
    .then(company => {
      res.status(200).json(company);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;
  Company.findOneAndRemove({ _id: id })
    .then(company => {
      res.status(200).json(company._id);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};
