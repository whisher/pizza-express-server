/*eslint max-len: ["error", { "code": 130 }]*/
"use strict";

const fs = require("fs");
const slugify = require("@sindresorhus/slugify");
const pump = require("pump");
const sharp = require("sharp");

const Dish = require("../models/dishes");

const updateDish = (id, data) => {
  return Dish.findByIdAndUpdate({ _id: id }, data, { new: true });
};

exports.create = (req, res, next) => {
  const dishData = new Dish({
    categoryId: req.body.categoryId,
    image: req.body.image,
    ingredients: req.body.ingredients,
    name: req.body.name,
    price: req.body.price,
    slug: slugify(req.body.name)
  });
  dishData
    .save()
    .then(dish => {
      res.status(200).json(dish);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.update = (req, res, next) => {
  const dishData = {
    categoryId: req.body.categoryId,
    image: req.body.image,
    ingredients: req.body.ingredients,
    name: req.body.name,
    price: req.body.price,
    slug: slugify(req.body.name)
  };
  updateDish(req.params.id, dishData)
    .then(dish => {
      res.status(200).json(dish);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.all = (req, res, next) => {
  Dish.find()
    .sort({ created: 1 })
    .then(dishes => {
      res.status(200).json(dishes);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.getByCategoryId = (req, res, next) => {
  Dish.find({ categoryId: req.params.categoryId })
    .sort({ created: 1 })
    .then(dishes => {
      res.status(200).json(dishes);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.getById = (req, res, next) => {
  Dish.findById(req.params.id)
    .then(dish => {
      res.status(200).json(dish);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;
  Dish.findOneAndDelete({ _id: id })
    .then(dish => {
      res.status(200).json(dish._id);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};
