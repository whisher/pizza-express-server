/*eslint max-len: ["error", { "code": 130 }]*/
"use strict";

const Category = require("../models/categories");

const updateCategory = (id, data) => {
  return Category.findByIdAndUpdate({ _id: id }, data, { new: true });
};

exports.create = (req, res, next) => {
  const categoryData = new Category({
    name: req.body.name
  });
  categoryData
    .save()
    .then(category => {
      res.status(200).json(category);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.update = (req, res, next) => {
  const categoryData = {
    name: req.body.name
  };
  updateCategory(req.params.id, categoryData)
    .then(category => {
      res.status(200).json(category);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.all = (req, res, next) => {
  Category.find()
    .sort({ created: 1 })
    .then(categories => {
      res.status(200).json(categories);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.getById = (req, res, next) => {
  Category.findById(req.params.id)
    .then(category => {
      res.status(200).json(category);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;
  Category.findOneAndRemove({ _id: id })
    .then(category => {
      res.status(200).json(category._id);
    })
    .catch(error => {
      res.status(500).json(error);
    });
};
