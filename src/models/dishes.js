/*eslint max-len: ["error", { "code": 100 }]*/
"use strict";
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const dishSchema = mongoose.Schema({
  categoryId: { type: mongoose.Schema.Types.ObjectId, required: true },
  created: { type: Number, default: Date.now },
  image: { type: String, required: true },
  ingredients: { type: Array, default: [] },
  name: { type: String, index: true, unique: true, required: true },
  price: { type: Number, required: true },
  slug: { type: String, required: true },
  update: { type: Number, default: Date.now }
});

dishSchema.plugin(uniqueValidator);

module.exports = mongoose.model("Dish", dishSchema);
