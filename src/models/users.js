/*eslint max-len: ["error", { "code": 100 }]*/
"use strict";
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const userSchema = mongoose.Schema({
  avatar: { type: String, required: false },
  companyId: { type: String, required: false },
  created: { type: Number, default: Date.now },
  email: { type: String, index: true, unique: true, required: true },
  firstname: { type: String, required: true },
  lastname: { type: String, required: true },
  mobile: { type: String, required: true },
  password: { type: String, required: true },
  role: { type: String, required: true, default: "user" }
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("User", userSchema);
