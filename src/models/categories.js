/*eslint max-len: ["error", { "code": 100 }]*/
"use strict";
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const categorySchema = mongoose.Schema({
  created: { type: Number, default: Date.now },
  name: { type: String, index: true, unique: true, required: true },
  update: { type: Number, default: Date.now }
});

categorySchema.plugin(uniqueValidator);

module.exports = mongoose.model("Category", categorySchema);
