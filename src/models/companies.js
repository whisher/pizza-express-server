/*eslint max-len: ["error", { "code": 100 }]*/
"use strict";
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const companySchema = mongoose.Schema({
  created: { type: Number, default: Date.now },
  address: { type: String, required: true },
  image: { type: String, required: true },
  name: { type: String, index: true, unique: true, required: true },
  update: { type: Number, default: Date.now }
});

companySchema.plugin(uniqueValidator);

module.exports = mongoose.model("Company", companySchema);
