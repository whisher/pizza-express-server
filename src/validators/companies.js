"use strict";
const { Joi } = require("celebrate");

exports.create = {
  body: {
    address: Joi.string().required(),
    image: Joi.string().required(),
    name: Joi.string().required()
  }
};

exports.update = {
  body: {
    _id: Joi.string().required(),
    address: Joi.string().required(),
    image: Joi.string().required(),
    name: Joi.string().required()
  }
};
