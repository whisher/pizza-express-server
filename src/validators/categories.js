"use strict";
const { Joi } = require("celebrate");

exports.create = {
  body: {
    name: Joi.string().required()
  }
};

exports.update = {
  body: {
    name: Joi.string().required()
  }
};
