"use strict";
const { Joi } = require("celebrate");

exports.create = {
  body: {
    categoryId: Joi.string().required(),
    image: Joi.string().required(),
    ingredients: Joi.array().required(),
    name: Joi.string().required(),
    price: Joi.number().required()
  }
};

exports.update = {
  body: {
    _id: Joi.string(),
    categoryId: Joi.string().required(),
    image: Joi.string().required(),
    ingredients: Joi.array().required(),
    name: Joi.string().required(),
    price: Joi.number().required()
  }
};
