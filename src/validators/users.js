"use strict";
const { Joi } = require("celebrate");

exports.create = {
  body: {
    avatar: Joi.string().required(),
    email: Joi.string()
      .email()
      .required(),
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    mobile: Joi.number().required(),
    password: Joi.string().required(),
    role: Joi.string().required()
  }
};

exports.update = {
  body: {
    avatar: Joi.string().required(),
    email: Joi.string()
      .email()
      .required(),
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    mobile: Joi.string().required(),
    role: Joi.string().required()
  }
};

exports.signup = {
  body: {
    email: Joi.string()
      .email()
      .required(),
    firstname: Joi.string().required(),
    lastname: Joi.string().required(),
    mobile: Joi.number().required(),
    password: Joi.string().required()
  }
};

exports.login = {
  body: {
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string().required()
  }
};
